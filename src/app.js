'use strict';

// requirements
const express = require('express');
const Amount = require('./Amount');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send 
app.get('/', (req, res) => { 
    var balance = 1000;
    var { balance, transfered } = transfer(balance, req.query.amount);
    if (balance !== undefined || transfered !== undefined) { 
        res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + balance);
    } else {
        res.status(400).end('Insufficient funds. Your balance: ' + balance);
    }
});

// Transfer amount service
var transfer = (balance, _amount) => {
    var transfered = 0;
    let amount
    try {
        amount = new Amount(_amount).value
    } catch (e) {
        return { undefined, undefined };
    }
    if (amount <= balance) {
        balance = balance - amount;
        transfered = transfered + amount;
        return { balance, transfered };
    } else
        return { undefined, undefined }; // returning undefined instead of throwing a bug
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
