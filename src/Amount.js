class Amount {
  constructor(value) {
    value = Number(value);
    if (!Number.isNaN(value) && value > 0) {
      this.value = value;
      Object.freeze(this);  
    } else {
      throw new TypeError("Invalid Format");
    }
  }
}

module.exports = Amount;